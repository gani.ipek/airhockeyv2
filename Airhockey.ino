// Hinzufügen der Bibliothek des 16x2-LCD-Bildschirms und Festlegen der zu verwendenden Pins
#include <LiquidCrystal.h>
LiquidCrystal lcd(A6, A7 , A5 , A4 ,A3 ,A2); //RS,E,D4,D5,D6,D7,VDD ->5V , RW -> GND


// Wir haben zwei Sensoren zur Pannenerkennung. 
// Wir benutzen einen auf der rechten Seite des Tisches
// und den anderen auf der linken Seite des Tisches. 
// Ich befestige den Sensor auf der linken Seite am D2-Pin
// und die rechte Seite am D3-Pin.     
#define IRReceiver_Left 2//PİNİ BELİRLE
#define IRReceiver_Right 3//PİNİ BELİRLE

// Pin D13 für Reset-Taste
#define Reset_Button 13//PİNİ  BELİRLE

// Pin D13 zur Steuerung des Relais, für das die Luftpumpe abgeschaltet werden soll
#define Luftpomp_Button 11

// Es gibt 2 Scoreboard. Eine links und eine rechts. Jeder benötigt einen Latch-, 
// Clock- und Daten-Pin.
#define Scoreboard_Left_latchPin 8
#define Scoreboard_Left_clockPin 9
#define Scoreboard_Left_dataPin 10
#define Scoreboard_Right_latchPin 6
#define Scoreboard_Right_clockPin 4
#define Scoreboard_Right_dataPin 5

// Um die Scoreboard zu verwalten, definieren wir 2 Bytes und setzen 
// ihren Anfangswert auf 0.
byte Scoreboard_Left = 0, Scoreboard_Right = 0; 

// Wir können Sonderzeichen auf dem LCD-Bildschirm definieren. 
// Ich definiere Left_Arrow und Right_Arrow, um die Gewinnerseite 
// mit dem Pfeil zu zeigen, nachdem das Spiel vorbei ist. Da "ä" nicht 
// definiert ist, füge ich es erneut als Sonderzeichen hinzu.
byte Left_Arrow[8] = {
  0b00000,
  0b00100,
  0b01100,
  0b11111,
  0b11111,
  0b01100,
  0b00100,
  0b00000
};
byte Right_Arrow[8] = {
  0b00000,
  0b00100,
  0b00110,
  0b11111,
  0b11111,
  0b00110,
  0b00100,
  0b00000
};
byte ae_for_Deutsch[8] = {
  0b01010,
  0b00000,
  0b01110,
  0b00001,
  0b01111,
  0b10001,
  0b01111,
  0b00000
};

//Während des Spiels zeigen wir die Zeit auf dem Bildschirm. Also weise ich 
//2 Variablen in Sekunden und Minuten zu.
int Second=0,Minute=0;

//Um die Uhrzeit anzuzeigen, verwenden wir die im Arduino verfügbaren Timer. 
//Hier wird timer1 interrupt mit 1 Hz ausgelöst. Dies bedeutet, dass die 
//Funktion in den ausgefallenen Klammern in Intervallen von 1 Sekunde 
//ausgeführt wird.
ISR(TIMER1_COMPA_vect){   
  Second = Second + 1;
  if(Second >59){
    Minute = Minute + 1;
    Second=0;
  }
}

// Rechte und linke Sensoridentifikation und Anfangswerte Null
int IRReceiver_Left_State = 0, IRReceiver_Left_lastState=0;               
int IRReceiver_Right_State = 0, IRReceiver_Right_lastState=0;   
int Reset_Button_State = 0, Reset_Button_lastState=0; 


//Der Sensor drückt beim Passieren der Kugel vor dem Sensor mehr als 0. 
//Dank dieser Klasse werden beide Treffer aufgezeichnet und bei mehr als 
//0 Treffern als ein einziges Tor gewertet.
class gamescore {
  public:
    int score = 0;
    bool scored = true;
    int finish = 7;
  void check_signal(bool state) {    
      if (state == false and scored){
        score = score + 1;
        scored = false;
      }     
      else if (not scored and state == true){
        scored = true;
      }
  }   
};

//Definieren von Variablen für die beiden Ziele
gamescore Player_Left;                                          
gamescore Player_Right;

//Funktion, die beim Neustart des Spiels verwendet wird
void Game_Reset (){
  digitalWrite(Luftpomp_Button, HIGH); //Luftpumpe schaltet ab
  lcd.clear(); //lcd Anzeige aufräumen
  lcd.setCursor(0, 0); // lcd position einstellung                      
  Player_Left.score= 0;   // Ergebnis auf Null setzen                                  
  Player_Right.score = 0; // Ergebnis auf Null setzen
  
  //Löscht alles, was auf der LED angezeigt wird
  Scoreboard_Left_Zahl_null();
  Scoreboard_Right_Zahl_null();

  //Das LCD zeigt "Spiel beginnt von neuem 5". Dann zählt er von fünf rückwärts.
  for(int i=5;i>=0;i--){
    lcd.setCursor(1,0);
    lcd.print("Spiel beginnt");
    lcd.setCursor(2,1);
    lcd.print("von neuem ");lcd.print(i);
    delay(500);
    lcd.clear();
    delay(500);
  }
  digitalWrite(Luftpomp_Button, LOW);  //Luftpomp schaltet ein
  Second = 0; //Null setzen
  Minute =0;  //Null setzen
 
}

//Funktion verwendet, wenn das Spiel vorbei ist
void Game_Ende(int Left,int Right,int Second,int Minute){
  digitalWrite(Luftpomp_Button, HIGH); //Luftpomp schaltet ab

    //Das LCD-Display zeigt die Punktzahl und die Spielzeit an
    lcd.setCursor(0,1);
    lcd.print(Left);
    lcd.setCursor(15,1);
    lcd.print(Right);
    lcd.setCursor(6,1);
    lcd.print(Minute/10);
    lcd.setCursor(7,1);
    lcd.print(Minute%10);
    lcd.setCursor(8,1);
    lcd.print(":");
    lcd.setCursor(9,1);
    lcd.print(Second/10);
    lcd.setCursor(10,1);
    lcd.print(Second%10);   
    delay(1000);

  //Text, der auf dem LCD-Bildschirm angezeigt wird, wenn die linke Seite gewinnt       
  if(Left>Right){
    lcd.setCursor(1,0);
    lcd.print("Left gewonnen");
    lcd.setCursor(1,1);
    lcd.write((uint8_t)0);
    delay(1000);
    lcd.clear();
    lcd.setCursor(1,0);
    lcd.print("Drucken Reset");
    lcd.setCursor(0,1);
    lcd.print("fur neue Spiel");
    delay(1000);
    lcd.clear();
 
  }
  //Text, der auf dem LCD-Bildschirm angezeigt wird, wenn die rechte Seite gewinnt
  else {
    lcd.setCursor(1,0);
    lcd.print("Right gewonnen"); 
    lcd.setCursor(14,1);
    lcd.write((uint8_t)1);
    delay(1000);
    lcd.clear();
    lcd.setCursor(1,0);
    lcd.print("Drucken Button");
    lcd.setCursor(0,1);
    lcd.print("fur neues Spiel");
    delay(1000);
    lcd.clear();
  }
          
}
int Goal_Counter(int Player_Left,int Player_Right){


//Welche Zahl auf den Scoreboards angezeigt werden soll 
if (Player_Left == 0){
  Scoreboard_Left_Zahl_null();
}
else if (Player_Left == 1){
  Scoreboard_Left_Zahl_ein();
}
else if (Player_Left == 2){
  Scoreboard_Left_Zahl_zwei();
}
else if (Player_Left == 3){
  Scoreboard_Left_Zahl_drei();
}
else if (Player_Left == 4){
  Scoreboard_Left_Zahl_vier();
}
else if (Player_Left == 5){
  Scoreboard_Left_Zahl_funf();
}
else if (Player_Left == 6){
  Scoreboard_Left_Zahl_sechs();
}
else if (Player_Left == 7){
  Scoreboard_Left_Zahl_sieben();
}
else if (Player_Left == 8){
  Scoreboard_Left_Zahl_acht();
}
if (Player_Right == 0){
  Scoreboard_Right_Zahl_null();
}
else if (Player_Right == 1){
  Scoreboard_Right_Zahl_ein();
}
else if (Player_Right == 2){
  Scoreboard_Right_Zahl_zwei();
}
else if (Player_Right == 3){
  Scoreboard_Right_Zahl_drei();
}
else if (Player_Right == 4){
  Scoreboard_Right_Zahl_vier();
}
else if (Player_Right == 5){
  Scoreboard_Right_Zahl_funf();
}
else if (Player_Right == 6){
  Scoreboard_Right_Zahl_sechs();
}
else if (Player_Right == 7){
  Scoreboard_Right_Zahl_sieben();
}
else if (Player_Right == 8){
  Scoreboard_Right_Zahl_acht();
}
}

//Anfängliche Arduino-Einstellungen
void setup() {
  pinMode(Luftpomp_Button, OUTPUT);
  digitalWrite(Luftpomp_Button, HIGH);
  
  pinMode(Scoreboard_Left_latchPin, OUTPUT);
  pinMode(Scoreboard_Left_dataPin, OUTPUT);  
  pinMode(Scoreboard_Left_clockPin, OUTPUT);
  pinMode(Scoreboard_Right_latchPin, OUTPUT);
  pinMode(Scoreboard_Right_dataPin, OUTPUT);  
  pinMode(Scoreboard_Right_clockPin, OUTPUT);
  Scoreboard_Left = 0;
  Scoreboard_Register_Left();
  Scoreboard_Right = 0;
  Scoreboard_Register_Right();
  
  pinMode(IRReceiver_Left, INPUT);
  pinMode(IRReceiver_Right, INPUT);
  
  pinMode(Reset_Button, INPUT);

  //Funktionen zum Erstellen neuer Zeichen auf dem LCD-Bildschirm
  lcd.createChar(0, Left_Arrow);
  lcd.createChar(1, Right_Arrow);
  lcd.createChar(2, ae_for_Deutsch);
  lcd.begin(16, 2);
  
  First_Start();

  //Timer-Einstellung
    cli();    //interrept stoppen           
    TCCR1A = 0;// TCCR1A-Register 0 setzen
    TCCR1B = 0;// TCCR1B-Register 0 setzen
    TCNT1  = 0;//mit Zählerwert 0
     // Einstellen des OCRxA-Vergleichsregisters für 1 Hz
  //16 MHz osilatör,1Hz timer1 ın çalışma frekansı,1024 prescalar
    OCR1A = 15624;// = (16*10^6) / (1*1024) - 1 (Wert unter 65536)
//   Der CTC-Modus ist aktiviert.
    TCCR1B |= (1 << WGM12);
  //   CS10- und CS12-Bits für 1024 Vorskalierer
    TCCR1B |= (1 << CS12) | (1 << CS10);  
  // Aktivierung der Timer-Vergleichsschnittstelle
    TIMSK1 |= (1 << OCIE1A);
    sei();

    
  Serial.begin(9600);
}

//Dauerfunktion
void loop() {
Reset_Button_State = digitalRead(Reset_Button);
IRReceiver_Left_State = digitalRead(IRReceiver_Left);
IRReceiver_Right_State = digitalRead(IRReceiver_Right);

Player_Left.check_signal(IRReceiver_Left_State);
Player_Right.check_signal(IRReceiver_Right_State);

Goal_Counter(Player_Left.score,Player_Right.score);
  lcd.setCursor(2,0);
  lcd.print("Spiel  l");
  lcd.setCursor(10,0);
  lcd.write((uint8_t)2);
  lcd.setCursor(11,0);
  lcd.print("uft");
  lcd.setCursor(0,1);
  lcd.print(Player_Left.score);
  lcd.setCursor(15,1);
  lcd.print(Player_Right.score);
  lcd.setCursor(6,1);
  lcd.print(Minute/10);
  lcd.setCursor(7,1);
  lcd.print(Minute%10);
  lcd.setCursor(8,1);
  lcd.print(":");
  lcd.setCursor(9,1);
  lcd.print(Second/10);
  lcd.setCursor(10,1);
  lcd.print(Second%10);

//Spiel wird zurückgesetzt, wenn die Reset-Button gedrückt wird
if(Reset_Button_State == 1){
  Game_Reset();
}

if(Player_Left.score > Player_Left.finish or Player_Right.score > Player_Right.finish){
  int Second_Ende = Second;
  int Minute_Ende = Minute;
  while(true){
  Game_Ende(Player_Left.score,Player_Right.score,Second_Ende,Minute_Ende);
  Goal_Counter(Player_Left.score,Player_Right.score);
  Reset_Button_State = digitalRead(Reset_Button);
  if(Reset_Button_State == 1){
    Game_Reset();
    break;
}
}
}
}

//Funktion, die beim Starten des Spiels verwendet wird.
void First_Start(){
  for(int i=3;i>=0;i--){
    lcd.setCursor(1,0);
    lcd.print("Spiel beginnt");
    lcd.setCursor(7,1);
    lcd.print(i);
    delay(1000);
    lcd.clear();
  } 
  digitalWrite(Luftpomp_Button, LOW); 
  
}



//Senden der Nummer im Scoreboar
void Scoreboard_Register_Left()
{
   digitalWrite(Scoreboard_Left_latchPin, LOW);
   shiftOut(Scoreboard_Left_dataPin, Scoreboard_Left_clockPin, LSBFIRST, Scoreboard_Left);
   digitalWrite(Scoreboard_Left_latchPin, HIGH);
}

//Senden der Nummer im Scoreboar
void Scoreboard_Register_Right()
{
   digitalWrite(Scoreboard_Right_latchPin, LOW);
   shiftOut(Scoreboard_Right_dataPin, Scoreboard_Right_clockPin, LSBFIRST, Scoreboard_Right);
   digitalWrite(Scoreboard_Right_latchPin , HIGH);
}

//Wir haben versucht, die Scoreboard mit der Fehlermethode zu finden und aufzuzeichnen, welches 
//Bit mit welcher Zahl gedruckt wurde.
void Scoreboard_Left_Zahl_null(){
  Scoreboard_Left=0;
  Scoreboard_Register_Left();
  Scoreboard_Left=238;
  Scoreboard_Register_Left();
}
void Scoreboard_Left_Zahl_ein (){
  Scoreboard_Left=0;
  Scoreboard_Register_Left();
  Scoreboard_Left=34;
  Scoreboard_Register_Left();
}
void Scoreboard_Left_Zahl_zwei (){
  Scoreboard_Left=0;
  Scoreboard_Register_Left();
  Scoreboard_Left=214;
  Scoreboard_Register_Left();
}
void Scoreboard_Left_Zahl_drei (){
  Scoreboard_Left=0;
  Scoreboard_Register_Left();
  Scoreboard_Left=182;
  Scoreboard_Register_Left();
}
void Scoreboard_Left_Zahl_vier (){
  Scoreboard_Left=0;
  Scoreboard_Register_Left();
  Scoreboard_Left=58;
  Scoreboard_Register_Left();
}
void Scoreboard_Left_Zahl_funf (){
  Scoreboard_Left=0;
  Scoreboard_Register_Left();
  Scoreboard_Left=188;
  Scoreboard_Register_Left();
}
void Scoreboard_Left_Zahl_sechs (){
  Scoreboard_Left=0;
  Scoreboard_Register_Left();
  Scoreboard_Left=252;
  Scoreboard_Register_Left();
}
void Scoreboard_Left_Zahl_sieben (){
  Scoreboard_Left=0;
  Scoreboard_Register_Left();
  Scoreboard_Left=38;
  Scoreboard_Register_Left();
}
void Scoreboard_Left_Zahl_acht (){
  Scoreboard_Left=0;
  Scoreboard_Register_Left();
  Scoreboard_Left=254;
  Scoreboard_Register_Left();
}
void Scoreboard_Left_Zahl_neun (){
  Scoreboard_Left=0;
  Scoreboard_Register_Left();
  Scoreboard_Left=190;
  Scoreboard_Register_Left();
}
void Scoreboard_Right_Zahl_null (){
  Scoreboard_Right=0;
  Scoreboard_Register_Right();
  Scoreboard_Right=238;
  Scoreboard_Register_Right();
}
void Scoreboard_Right_Zahl_ein (){
  Scoreboard_Right=0;
  Scoreboard_Register_Right();
  Scoreboard_Right=132;
  Scoreboard_Register_Right();
}
void Scoreboard_Right_Zahl_zwei (){
  Scoreboard_Right=0;
  Scoreboard_Register_Right();
  Scoreboard_Right=218;
  Scoreboard_Register_Right();
}
void Scoreboard_Right_Zahl_drei (){
  Scoreboard_Right=0;
  Scoreboard_Register_Right();
  Scoreboard_Right=214;
  Scoreboard_Register_Right();

}
void Scoreboard_Right_Zahl_vier (){
  Scoreboard_Right=0;
  Scoreboard_Register_Right();
  Scoreboard_Right=180;
  Scoreboard_Register_Right();
}
void Scoreboard_Right_Zahl_funf (){
  Scoreboard_Right=0;
  Scoreboard_Register_Right();
  Scoreboard_Right=118;
  Scoreboard_Register_Right();
}
void Scoreboard_Right_Zahl_sechs (){
  Scoreboard_Right=0;
  Scoreboard_Register_Right();
  Scoreboard_Right=126;
  Scoreboard_Register_Right();
}
void Scoreboard_Right_Zahl_sieben (){
  Scoreboard_Right=0;
  Scoreboard_Register_Right();
  Scoreboard_Right=196;
  Scoreboard_Register_Right();
}
void Scoreboard_Right_Zahl_acht (){
  Scoreboard_Right=0;
  Scoreboard_Register_Right();
  Scoreboard_Right=254;
  Scoreboard_Register_Right();
}
void Scoreboard_Right_Zahl_neun (){
  Scoreboard_Right=0;
  Scoreboard_Register_Right();
  Scoreboard_Right=246;
  Scoreboard_Register_Right();
}